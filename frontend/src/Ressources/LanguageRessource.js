
export const LanguageRessources = {
    EN: {
        LoginRessource: {
            LoginHeader: "Welcome!",
            UserField: "Username",
            PasswordField: "Password",
            LoginButton: "Login",
            LoginWarning: "Login failed.",
            Register: "Not registered jet?",
        },
        RegisterRessource: {
            RegisterHeader: "Register",
            RegisterText: "Create your Account. It's free and only takes a minute.",
            UserField: "Username",
            InfoText: "We'll never share your data with anyone else.",
            PasswordField: "Password",
            PasswordConfirm: "Confirm Password",
            RegisterButton: "Register Now",
            LoginLink: "To the login"
        },
        CorePageRessource: {
            Points: "Points",
            PointsPC: "Points you get per click: ",
            ClickButton: "CLick me!",
            Generators: "Generators",
            LogoutButton: "Logout"
        },
        LanguageRessource: {
            Id: "EN",
            Repressentation: "English"
        }

    },
    DE: {
        LoginRessource: {
            LoginHeader: "Wilkommen!",
            UserField: "Benutzername",
            PasswordField: "Passwort",
            LoginButton: "Anmelden",
            LoginWarning: "Login fehlgeschlagen.",
            Register: "Noch nicht Registriert?",
        },
        RegisterRessource: {
            RegisterHeader: "Registrieren",
            RegisterText: "Erstelle deinen Account. Es ist kostenlos und dauert nur eine Minute!",
            UserField: "Benutzername",
            InfoText: "Wir teilen deine Daten mit niemandem sonst.",
            PasswordField: "Passwort",
            PasswordConfirm: "Passwort wiederholen",
            RegisterButton: "Registrieren",
            LoginLink: "Zum Login"
        },
        CorePageRessource: {
            Points: "Punkte",
            PointsPC: "Punkte die du per Klick bekommst: ",
            ClickButton: "Klick mich!",
            Generators: "Generatoren",
            LogoutButton: "Abmelden"
        },
        LanguageRessource: {
            Id: "DE",
            Repressentation: "Deutsch"
        }

    },
}

export const Languages = {
    DE: "DE",
    EN: "EN",
    DEFAULT: "DE",
}